package ru.t1.panasyuk.tm.api;

import ru.t1.panasyuk.tm.model.Task;

public interface ITaskService extends ITaskRepository {

    Task create(final String name, final String description);

    Task create(String name);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

}