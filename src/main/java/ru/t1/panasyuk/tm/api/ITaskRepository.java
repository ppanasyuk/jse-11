package ru.t1.panasyuk.tm.api;

import ru.t1.panasyuk.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    Task add(Task task);

    void clear();

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

}