package ru.t1.panasyuk.tm.api;

import ru.t1.panasyuk.tm.model.Project;

public interface IProjectService extends IProjectRepository {

    Project create(String name, String description);

    Project create(String name);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

}